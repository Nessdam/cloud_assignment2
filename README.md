# Cloud_Assignment2
This is a RESTful api developed in golang as part of an assignment at NTNU


First things first, in order to run this program you will need to clone it.
then, either make a new firebase project and then create a collection 
or create a new collection in an existing project

The collection you will either have to be named webhooks or you will have to
change the value for the const, collection, on line 25 in the externals file to match with your collection name

You will also need to make an service account and download the service account keys if you do not 
already have one. There are instructions on how to do so [here](https://firebase.google.com/docs/admin/setup#initialize_the_sdk)

You will then need to move the key file into the project folder (not cmd wher main is) and change the value of the const AdminSDK
to be ./YourKeyFileName.json
if you are planning on running your project from the cmd folder you will probbably need to add an extra '.' before the '/'

For testing webhooks [this](https://webhook.site/) website is useful

You will have to use your own gitlab authorization key

## Additional Info
I did not implement the languages endpoint

If a user does not, or inputs a wrong value for limit in commits, the value used and the value sent through the webhooks will be set to 5

There are many projects that have the same name, i have therefore chosen to pick the project that was created first 
as when looking at projects with the same name the first seems to usually have more content. This might not always be true though

