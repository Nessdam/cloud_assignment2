module getlab

go 1.13

require (
	cloud.google.com/go/firestore v1.0.0
	cloud.google.com/go/storage v1.1.2 // indirect
	firebase.google.com/go v3.10.0+incompatible
	google.golang.org/api v0.13.0
)
