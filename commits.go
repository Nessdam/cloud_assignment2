package getlab

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"sort"
	"strconv"
	"time"
)

//-------------------------------------------------------------------------------------------------------
//											Consts
//-------------------------------------------------------------------------------------------------------

const getCommitsAddOn string = "/repository/commits"
const defaultLimitValue string = "5"
const defaultLimValue int = 5

//-------------------------------------------------------------------------------------------------------
//											Structs
//-------------------------------------------------------------------------------------------------------

//countCommit is a struct that is only used to temporarily contain some data from the commits
//  so the number of commits can be counted
type countCommit struct {
	ID string `json:"id"`
}

//Output is the tdelivery struct and essentially a formating struct to get the correct delivery format
type Output struct {
	Content []InOutput `json:"repos"`
	Token   bool       `json:"auth"`
}

//InOutput is the content for the delivery struct containing the data for each of the lim projects
type InOutput struct {
	PathWithNamespace string `json:"path_with_namespace"`
	NumCommits        int    `json:"commits"`
}

//-------------------------------------------------------------------------------------------------------
//											Handler
//-------------------------------------------------------------------------------------------------------

//HandleCommits the handler resposible for recieving calls to the commits endpoint
func HandleCommits(w http.ResponseWriter, r *http.Request) {
	var projectNumbers2 AllProjectData // Struct to collect all the projects
	var output Output                  // This is the delivery struct

	var projectDataURL string
	var commitsURLpart2 string

	limit := r.FormValue("limit") // gets the limit value from the url
	if limit == "" {
		limit = defaultLimitValue
	}
	lim, er := strconv.Atoi(limit) // Converts limit to int
	if er != nil {
		log.Println(er)
		lim = defaultLimValue
	}
	if lim < 1 || lim > 9 {
		lim = defaultLimValue
	}

	authtoken := r.FormValue("private_token") // Gets private token value from URL

	commitsURLpart1 := getProjectsURL + "/" // Adjusts the const with necessary value

	if authtoken == "" { // Creates the get URLs depending on auth token or not
		projectDataURL = getProjectsURL + "?per_page=100&page="
		commitsURLpart2 = getCommitsAddOn + "?per_page=100" + "&page="
		output.Token = false
	} else {
		projectDataURL = getProjectsURL + "?per_page=100&private_token=" + authtoken + "&page="
		commitsURLpart2 = getCommitsAddOn + "?per_page=100&private_token=" + authtoken + "&page="
		output.Token = true
	}

	err := getProjects(projectDataURL, &projectNumbers2) // Calls external function to get project data by reference into slice of struct
	if err != nil {                                      // returns an error
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	//-------------------------------------------------------------------------------------------------------
	//										Get and sort commits
	//-------------------------------------------------------------------------------------------------------

	for i := 0; i < len(projectNumbers2.Result); i++ { // For each of the retrieved projects
		var pageOfCommits []countCommit // Declares a struct to get some data into so the number of commitscan be calculated

		pageCount := 1

		for whileBool := true; whileBool == true; pageCount++ { // While there still are pages

			commitsURL := commitsURLpart1 + strconv.Itoa(projectNumbers2.Result[i].ID) + commitsURLpart2 + strconv.Itoa(pageCount) // Generate page specific URL

			res, err := http.Get(commitsURL) // Get the data
			if err != nil {
				log.Println(err) // There are projects that dont allow access regardless of authorization and if anything could go wrong it should already have done so
			}

			err = json.NewDecoder(res.Body).Decode(&pageOfCommits)
			if err != nil {
				log.Println(err)
			}

			projectNumbers2.Result[i].NumCommits += len(pageOfCommits) // Adds number of commits on the page to the sum of the project

			if len(pageOfCommits) < 100 { // If this is the last page
				whileBool = false
			}
			defer res.Body.Close() // closes the body of res to avoid memory leak
		}
	}

	sort.Slice(projectNumbers2.Result, func(i, j int) bool { // Sort the slice with projects from most to least commits
		return projectNumbers2.Result[i].NumCommits > projectNumbers2.Result[j].NumCommits
	})

	for i := 0; i < lim; i++ { // For lim add the next project with the highest commits
		var part InOutput
		part.PathWithNamespace = projectNumbers2.Result[i].PathWithNamespace
		part.NumCommits = projectNumbers2.Result[i].NumCommits

		output.Content = append(output.Content, part)
	}
	//-------------------------------------------------------------------------------------------------------
	//											Web Hooks
	//-------------------------------------------------------------------------------------------------------

	var hookInfo Hookresponse // Creates webhook response struct and gives it the needed values
	hookInfo.Event = "commits"
	hookInfo.Param = "limit: " + limit + ", authToken: " + strconv.FormatBool(output.Token)
	hookInfo.TimeStamp = time.Now().String()

	var buf = new(bytes.Buffer) // Creates bufer and encoder
	encoder := json.NewEncoder(buf)

	for i := 0; i < len(hookes); i++ {
		if hookes[i].Event == "commits" { // For each hook that wants commits
			encoder.Encode(hookInfo)                                    //encode into buffer
			_, err := http.Post(hookes[i].URL, "application/json", buf) // And post to the url of the wehook
			if err != nil {
				http.Error(w, "Could not post to webhook URL", http.StatusMethodNotAllowed)
			}
		}
	}

	// Encodes and prints data to server
	var buffer = new(bytes.Buffer)
	encoder = json.NewEncoder(buffer)
	encoder.Encode(output)

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	io.Copy(w, buffer)
}
