package getlab

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"strings"
	"time"
)

var id int = 0

//-------------------------------------------------------------------------------------------------------
//											Structs
//-------------------------------------------------------------------------------------------------------

//Webhook Struct containing the webhook info
type webhook struct {
	ID          string `json:"id"` //id is not sendt
	Event       string `json:"event"`
	URL         string `json:"url"`
	PointInTime string `json:"time"`
}

//DocID struct to print the webhook id
type DocID struct {
	DoID string `json:"id"`
}

//-------------------------------------------------------------------------------------------------------
//										Webhook handler
//-------------------------------------------------------------------------------------------------------

//HandleWebhooks the handler responsible for handling requests to webhook endpoint
func HandleWebhooks(w http.ResponseWriter, r *http.Request) {
	var hookid int // Int used to contain the index of the webhook in the webhook array

	parts := strings.Split(r.URL.Path, "/") //Splits url by '/' to retrieve the webhook id

	switch r.Method { // Switch to correctly handle the different request types

	//-------------------------------------------------------------------------------------------------------
	//											Case GET
	//-------------------------------------------------------------------------------------------------------
	case "GET":

		var buffer = new(bytes.Buffer)
		encoder := json.NewEncoder(buffer)

		if len(parts) != 5 { // If there is no webhook id
			encoder := json.NewEncoder(buffer) //encode and send the webhook struct
			encoder.Encode(hookes)
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			io.Copy(w, buffer)
		} else {

			for i := 0; i < len(hookes); i++ { // For each webhook in the slice
				if hookes[i].ID == parts[4] { // see if the id matches and set hookid to index value
					hookid = i
				} else if i == len(hookes)-1 && hookes[i].ID != parts[4] { // If loops through whole slice and still not found
					http.Error(w, "Webhook not found", http.StatusNotFound)
					return
				}
			}

			encoder.Encode(hookes[hookid]) // encode and send struct
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			io.Copy(w, buffer)
		}

	//-------------------------------------------------------------------------------------------------------
	//										Case POST
	//-------------------------------------------------------------------------------------------------------
	case "POST":
		var hook webhook // predeclares structs
		var docid DocID

		err := json.NewDecoder(r.Body).Decode(&hook) // Decodes res body into struct
		if err != nil {                              // if unable to decode
			http.Error(w, "Unable to decode body data", http.StatusNotAcceptable)
			return
		}
		if hook.Event != "commits" && hook.Event != "issues" && hook.Event != "status" { // If illegal value
			http.Error(w, "Value of event does not match any registered values", http.StatusBadRequest)
			return
		}
		hook.PointInTime = time.Now().String()                   // Saves point in time as string into struct
		_, _, err = Client.Collection(collection).Add(Ctx, hook) // Adds struct to the database
		if err != nil {                                          // Could not add to database
			http.Error(w, "Could not add data to server", http.StatusServiceUnavailable)
			return
		}

		RetrieveDataFromDB() // Gets webhooks from database

		for i := 0; i < len(hookes); i++ { // For each webhook
			if hookes[i].PointInTime == hook.PointInTime { // checks if the time matches the time of the created webhook
				hook.ID = hookes[i].ID // ass time is specific down to location and nanosecond two entries with same time should not occur
			}
		}
		docid.DoID = hook.ID // Sets id to be id of the webhook with matching time

		var buffer = new(bytes.Buffer) //Encodes and prints id to the service
		encoder := json.NewEncoder(buffer)
		encoder.Encode(docid)
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		io.Copy(w, buffer)

	//-------------------------------------------------------------------------------------------------------
	//										Case DELETE
	//-------------------------------------------------------------------------------------------------------
	case "DELETE":
		for i := 0; i < len(hookes); i++ { // Same as in get with id, goes through finds matching and gets index
			if hookes[i].ID == parts[4] {
				hookid = i
			}
		}
		//Collection(collection).  collection + "/" +

		_, err := Client.Collection(collection).Doc(hookes[hookid].ID).Delete(Ctx) // Deletes a specific document
		if err != nil {
			http.Error(w, "Could not delete entry", http.StatusUnprocessableEntity)
			return
		}

		hookes = nil // Empty slice and get the webhooks from the database to update local storage
		RetrieveDataFromDB()

	default:
		http.Error(w, "Only Get, Post and Delete are supported", http.StatusBadRequest)
		return
	}
}
