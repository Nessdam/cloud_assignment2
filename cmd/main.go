package main

import (
	"context"
	"getlab"
	"log"
	"net/http"
	"os"
	"time"

	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
)

//Const string for base endpoint string
const basePoint string = "/repocheck/" + getlab.Version

func main() {
	getlab.Uptime = time.Now() //saves timepoint of main startup

	var err error

	//Sets up server connection
	getlab.Ctx = context.Background()
	getlab.Sa = option.WithCredentialsFile(getlab.AdminSDK)
	getlab.App, err = firebase.NewApp(getlab.Ctx, nil, getlab.Sa)
	if err != nil {
		log.Println(err)
	}
	getlab.Client, err = getlab.App.Firestore(getlab.Ctx)
	if err != nil {
		log.Println(err)
	}
	defer getlab.Client.Close()

	getlab.RetrieveDataFromDB() // Gets the registered webhooks from the server into a local slice for faster access

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	http.HandleFunc(basePoint+"/", getlab.HandleBadRequests)
	http.HandleFunc(basePoint+"/commits", getlab.HandleCommits)
	http.HandleFunc(basePoint+"/issues", getlab.HandleIssues)
	http.HandleFunc(basePoint+"/status", getlab.HandleStatus)
	http.HandleFunc(basePoint+"/webhooks", getlab.HandleWebhooks)
	http.HandleFunc(basePoint+"/webhooks/", getlab.HandleWebhooks)

	log.Fatal(http.ListenAndServe(":"+port, nil))
}
