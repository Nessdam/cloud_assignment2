package getlab

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"cloud.google.com/go/firestore"
	firebase "firebase.google.com/go"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

//-------------------------------------------------------------------------------------------------------
//											Consts
//-------------------------------------------------------------------------------------------------------

//AdminSDK is the file name of the admin key file for firebase, I had to put two dots before the /
const AdminSDK string = "./cloudassignment2-c3ba3-firebase-adminsdk-76fty-772bd78c17.json"

//Name string of the collection in the database
const collection string = "webhooks"

//The id of the project
//const projectID string = "cloudassignment2-c3ba3"

//URL used to get the data from projects
const getProjectsURL string = "https://git.gvk.idi.ntnu.no/api/v4/projects"

//Version string that represents the product version
const Version string = "v1"

//Uptime an time entity used to contain the startup time of the service
var Uptime time.Time

//App is the firestore app
var App *firebase.App

//Client is the firestore client
var Client *firestore.Client

//Ctx is the context used by firestore functions
var Ctx context.Context

//Sa used by firestore
var Sa option.ClientOption

//This is the slice of structs containing the local copies of the webhooks
var hookes []webhook

//AllProjectData is the struct used to contain the data of every project
type AllProjectData struct {
	Result []projectCommitData `json:"result"`
}

//-------------------------------------------------------------------------------------------------------
//											Structs
//-------------------------------------------------------------------------------------------------------

//ProjectCommitData is the struct used to get data from the api
type projectCommitData struct {
	ID                int    `json:"id"`
	Name              string `json:"name"`
	PathWithNamespace string `json:"path_with_namespace"`
	NumCommits        int    `json:"commits"`
}

//Hookresponse is the struct used to deliver data to the webhooks
type Hookresponse struct {
	Event     string `json:"event"`
	Param     string `json:"params"`
	TimeStamp string `json:"time"`
}

//-------------------------------------------------------------------------------------------------------
//										External Functions
//-------------------------------------------------------------------------------------------------------

//getProjects is the function that
func getProjects(url string, allProjects *AllProjectData) error {
	var pageOfProjects []projectCommitData // Struct used to get and contain one page of projects

	var res *http.Response // Predeclaration
	var err error

	pageCount := 1
	for whileBool := true; whileBool == true; pageCount++ { // While not on last page
		getURL := url + strconv.Itoa(pageCount) // Speciefies page of url

		res, err = http.Get(getURL) //Get one page of projects
		if err != nil {
			return err
		}
		err = json.NewDecoder(res.Body).Decode(&pageOfProjects) //decoes
		if err != nil {
			return err
		}

		if len(pageOfProjects) < 100 { // if on last page stop loop
			whileBool = false
		}

		for i := 0; i < len(pageOfProjects); i++ { // Append each project to the parameter struct
			allProjects.Result = append(allProjects.Result, pageOfProjects[i])
		}
		defer res.Body.Close()
	}
	return err
}

//RetrieveDataFromDB is a function used to retrieve all webhooks from the database into a global slice of struct
func RetrieveDataFromDB() {
	iter := Client.Collection(collection).Documents(Ctx) //The document collection in the database

	for { //while
		doc, err := iter.Next()   //flips through the documents
		if err == iterator.Done { // if no more documents jump out of function
			break
		}
		if err != nil {
			log.Println(err)
		}

		var temp webhook // Creates a temmp struct and sends the data fro the doc into it
		doc.DataTo(&temp)
		temp.ID = doc.Ref.ID // Uptades the id of the temp

		alreadyExist := false
		for i := 0; i < len(hookes); i++ { //Loops and checks if the webhook already exist
			if temp.ID == hookes[i].ID {
				alreadyExist = true
			}
		}

		if alreadyExist == false { // if it does not add it to the slice of struct
			hookes = append(hookes, temp)
		}

	}
}

//HandleBadRequests handles bad requests
func HandleBadRequests(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "Bad Request", http.StatusBadRequest)
}
