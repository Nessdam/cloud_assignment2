package getlab

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"
)

//Status struct, used for delivering status info to service
type Status struct {
	Gitlab   string `json:"gitlab"`
	Database string `json:"database"`
	Uptime   string `json:"uptime"`
	Version  string `json:"version"`
}

const randRequest string = "https://git.gvk.idi.ntnu.no/api/v4/projects?private_token=Sa4MVZHMjtzHScJQZVjg&per_page=1&page=4"

//-------------------------------------------------------------------------------------------------------
//											Status Handler
//-------------------------------------------------------------------------------------------------------

//HandleStatus is the handler that accepts the status endpoint requests
func HandleStatus(w http.ResponseWriter, r *http.Request) {
	var status Status // Delivery struct declaration

	res, err := http.Get(randRequest) //Random request to check status of api
	if err != nil {
		log.Println(err) // Dont really need to to anything
	}
	status.Gitlab = res.Status // Updates status

	_, err = Client.Collection(collection).Doc(hookes[0].ID).Get(Ctx) //Tries to get to discover state of DB
	if err != nil {
		status.Database = "503 Service unavailable"
	} else {
		status.Database = "200 OK"
	}

	status.Uptime = time.Since(Uptime).String() //Measuers time since service startup

	status.Version = Version // sets version
	defer res.Body.Close()

	var hookInfo Hookresponse // Webhook handling
	hookInfo.Event = "status"
	hookInfo.Param = ""
	hookInfo.TimeStamp = time.Now().String()
	var buf = new(bytes.Buffer)
	encoder := json.NewEncoder(buf)
	for i := 0; i < len(hookes); i++ {
		if hookes[i].Event == "status" {
			encoder.Encode(hookInfo)
			_, err := http.Post(hookes[i].URL, "application/json", buf)
			if err != nil {
				log.Println(err)
			}
		}
	}

	var buffer = new(bytes.Buffer) //Sends data to service
	encoder = json.NewEncoder(buffer)
	encoder.Encode(status)
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.Copy(w, buffer)
}
