package getlab

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"time"
)

//-------------------------------------------------------------------------------------------------------
//											Structs
//-------------------------------------------------------------------------------------------------------

//Payload for getting data from get body
type Payload struct {
	Project string `json:"project"`
}

//AllUsers is the delivery struct for type users
type AllUsers struct {
	Users          []User `json:"author"`
	Authorizationu bool   `json:"auth"`
}

//User is the struct containing the data for each user
type User struct {
	Username string `json:"username"`
	Count    int    `json:"count"`
}

//AllLabels is the delivery struct for type labels
type AllLabels struct {
	Labels         []Labels `json:"labels"`
	Authorizationl bool     `json:"auth"`
}

//Labels is the struct conataining the data for each label
type Labels struct {
	Label string `json:"label"`
	Count int    `json:"count"`
}

//IssuesData is the struct used to retrieve all neccessary data for both users and labels
type IssuesData struct {
	IssueLabels []string `json:"labels"`
	IssueUsers  User     `json:"author"`
}

//-------------------------------------------------------------------------------------------------------
//											Issues Handler
//-------------------------------------------------------------------------------------------------------

//HandleIssues is the handler that accepts the issues endpoint requests
func HandleIssues(w http.ResponseWriter, r *http.Request) {
	var labelOutput AllLabels // Declaring the delivery structs for labels and users
	var userOutput AllUsers

	var allIssueData []IssuesData // Declares slice of struct used to get the data
	var payload Payload           // Struct used to get the body of the request
	var projects AllProjectData   // Struct used to retrieve all the projects

	var projectID int //Predeclared values
	var projectsURL string
	var issuesURL string

	kind := r.FormValue("type") // Get the type and authToken from the URL
	authtoken := r.FormValue("private_token")

	err := json.NewDecoder(r.Body).Decode(&payload) // Retrieve the project name in from the request body
	if err != nil {
		http.Error(w, "Empty or unprocessable body", http.StatusNoContent)
		return
	}
	if payload.Project == "" { // Extra check
		http.Error(w, "Empty or unprocessable body", http.StatusNoContent)
		return
	}
	if kind != "users" && kind != "labels" { // Type is necessary
		http.Error(w, "Missing type", http.StatusBadRequest)
		return
	}

	if authtoken == "" { // Depending on authorization values are assigned
		projectsURL = getProjectsURL + "?per_page=100&page="
		userOutput.Authorizationu = false
		labelOutput.Authorizationl = false

	} else {
		projectsURL = getProjectsURL + "?per_page=100&private_token=" + authtoken + "&page="
		userOutput.Authorizationu = true
		labelOutput.Authorizationl = true
	}

	err = getProjects(projectsURL, &projects) // Gets all projects through external function
	if err != nil {                           // Error means either spi is unaveilable or wrong authorization
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	// A for loop that loops and finds a project that has matching name and gets the id but continues
	// to loop to find tha last project in the project slice with that name as the project slice
	// is inverted of the gitlab and the earlier projects have a higher chance of containing issues
	for i := 0; i < len(projects.Result); i++ {
		if payload.Project == projects.Result[i].Name {
			projectID = projects.Result[i].ID
		}
	}
	if authtoken == "" { // Makes urls after getting id
		issuesURL = getProjectsURL + "/" + strconv.Itoa(projectID) + "/issues?per_page=100&page="
	} else {
		issuesURL = getProjectsURL + "/" + strconv.Itoa(projectID) + "/issues?per_page=100&private_token=" + authtoken + "&page="
	}

	//-------------------------------------------------------------------------------------------------------
	//											Get issues
	//-------------------------------------------------------------------------------------------------------

	// For loop(basically a while) that gets all issues for the named project
	pageCount := 1
	for whileBool := true; whileBool == true; pageCount++ {
		var issuePageData []IssuesData                // A temporary struct to store one page of issues
		getURL := issuesURL + strconv.Itoa(pageCount) // constructs Get URL

		res, err := http.Get(getURL) // Gets issues
		if err != nil {
			http.Error(w, "Could not find project issues", http.StatusNotFound)
			return
		}

		err = json.NewDecoder(res.Body).Decode(&issuePageData)
		if err != nil {
			http.Error(w, "Could not process list of issues", http.StatusUnprocessableEntity)
		}

		for i := 0; i < len(issuePageData); i++ { //Append the slice of issues to the slice off all issues
			allIssueData = append(allIssueData, issuePageData[i])
		}

		if len(issuePageData) < 100 { // While not last page
			whileBool = false
		}
		defer res.Body.Close() // Avoid leak
	}

	var buffer = new(bytes.Buffer) // Predeclaration as they used in the if else
	encoder := json.NewEncoder(buffer)

	//-------------------------------------------------------------------------------------------------------
	//											Data Processer
	//-------------------------------------------------------------------------------------------------------

	if kind == "labels" {
		for i := 0; i < len(allIssueData); i++ { // For all issues
			for j := 0; j < len(allIssueData[i].IssueLabels); j++ { // For each label in an issue
				alreadyExist := false                          // Check bool
				for k := 0; k < len(labelOutput.Labels); k++ { // For each label in the output struct
					if allIssueData[i].IssueLabels[j] == labelOutput.Labels[k].Label { // If this label is in the delivery list
						alreadyExist = true
						labelOutput.Labels[k].Count++ // Dont add me and increase count
					}
				}
				if alreadyExist == false { // If im unique add me and increase count
					var tempLabel Labels
					tempLabel.Count++
					tempLabel.Label = allIssueData[i].IssueLabels[j]
					labelOutput.Labels = append(labelOutput.Labels, tempLabel)
				}
			}
		}
		encoder.Encode(labelOutput) // Encode results

	} else {
		for i := 0; i < len(allIssueData); i++ { // For each issue
			alreadyExist := false
			for j := 0; j < len(userOutput.Users); j++ { // For each user in delivery struct
				if allIssueData[i].IssueUsers.Username == userOutput.Users[j].Username { // If i match an user in the delivery struct
					alreadyExist = true // Dont add me and increase count
					userOutput.Users[j].Count++
				}
			}
			if alreadyExist == false { // Add me if not
				var tempUser User
				tempUser.Username = allIssueData[i].IssueUsers.Username
				userOutput.Users = append(userOutput.Users, tempUser)
			}
		}

		encoder.Encode(userOutput) // Encode
	}
	//-------------------------------------------------------------------------------------------------------
	//											WebHooks
	//-------------------------------------------------------------------------------------------------------
	var hookInfo Hookresponse
	hookInfo.Event = "issues"
	hookInfo.Param = "type: " + kind + ", project: " + payload.Project + ", authToken: " + strconv.FormatBool(userOutput.Authorizationu)
	hookInfo.TimeStamp = time.Now().String()
	var buf = new(bytes.Buffer)
	encoder = json.NewEncoder(buf)

	for i := 0; i < len(hookes); i++ { // For each webhook
		if hookes[i].Event == "issues" { // If type matches
			encoder.Encode(hookInfo)
			_, err := http.Post(hookes[i].URL, "application/json", buf) // post to webhook URL
			if err != nil {
				http.Error(w, "Could not post to webhook URL", http.StatusMethodNotAllowed)
			}
		}
	}

	w.Header().Add("Content-Type", "application/json")
	//w.WriteHeader(http.StatusOK)

	io.Copy(w, buffer)
}
